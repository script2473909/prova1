#!/bin/bash

echo -e "\nquando dormires, minha bela tenebrosa"
sleep 1

echo -e "\nNo fundo da tumba feita de marmore negro"
sleep 1

echo -e "\ne nao tiveres por alcova e por mansão"
sleep 1

echo -e "\nmais que uma cova pluviosa e uma fossa oca"
sleep 1

echo -e "\nquando a pedra, oprimindo o teu peito medroso"
sleep 1

echo -e "\ne teus flacos que abranda um charmoso descaso"
sleep 1

echo -e "\nimpedir teu coração de arfar e querer"
sleep 1

echo -e "\ne teus pés de correr seu curso aventuroso"
sleep 1

echo -e "\no tumulo, guardião do meu sonho infinito"
sleep 1

echo -e "\nnessas longas noites de que o sono e banido\n"
sleep 1

echo -e "Charles Baudelaire - as flores do mal\n"
