#!/bin/bash

echo -e "\nuso de memoria\n"
free
read

echo -e "\nuso de disco\n"
df -h
read

echo -e "\nusuarios logados\n"
who
read

echo -e "\nnumero de arquivos na pasta atual\n"
ls -l | wc -l
read

echo -e "\nInformações de IP\n"
ip addr show
read

echo -e "\ntabela de rotas\n"
ip route
read

echo -e "\nOnde estou?\n"
pwd
read

echo -e "\nlista dos processos de todos os usuarios\n"
ps -aux
read

