#!/bin/bash

echo -e "\nExibindo variaveis globais\n"

echo "\$BASH = $BASH"

echo "\$BASH_VERSION = $BASH_VERSION"

echo "\$HOME = $HOME"

echo "\$PWD = $PWD"

echo "\$LOGNAME = $LOGNAME"

echo "\$XDG_RUNTIME_DIR = $XDG_RUNTIME_DIR"

echo "\$MAIL = $MAIL"

echo "\$OLDPWD = $OLDPWD"

echo "\$USER = $USER"

echo "\$TERM = $TERM"
