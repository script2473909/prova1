#!/bin/bash

echo -e "\nExistem 3 formas de criar ex: 'var=valor'\n"
read

echo -e "\nNesse exemplo a variavel e criada atribuindo um valor a linha de comando\n"
read

echo -e "\nArgumento de linha de comando 'var=$1'"
read

echo -e "\nNesse modo o script e chamado com argumento na linha de comando\n"
read

echo -e "\npor meio do mando read\n"
read

echo -e "\npedimos a interação do usuario, essa forma se assemelha ao input\n"
read

echo -e "\nA diferença basica é que no read pedimos a interação com o usuario, porem está sucestivel a erros de digitação do usuario\n"
read

echo -e "\nQuando recemos  parametro na linha de comando, o processo se torna altomatizado sem necessidade da interação do usuario\n"

